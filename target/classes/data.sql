INSERT INTO auth(username,password,role,is_active,created_at)
    VALUES ('admin','admin123',true,true,now()),
           ('user01','user123',true,false,now()),
           ('lexi','lexi123',true,true,now());

INSERT INTO user_detail(account_id,account,name,email,phone,address,created_at)
    VALUES (1,1,'administrator','admin@admin.com','089234273432','yogyakarta',now()),
           (2,2,'user 01', 'user01@gmail.com','081293829523','jakarta',now());

INSERT INTO building(name,address,owner,phone,is_active,created_at)
    VALUES('Panggung Krapyak', 'bantul', 'budi','089866812736',true, now()),
          ('Dalem Brotokusuman','yogyakarta', 'sultan','085928374234',true, now());

INSERT INTO room(building,building_id,name,price,is_active,created_at)
    VALUES(1,1,'Arjuna',500000,true,now()),
          (1,1,'Yudistira',600000,true,now()),
          (2,2,'Nakula',700000,true,now());
