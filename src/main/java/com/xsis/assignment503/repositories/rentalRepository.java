package com.xsis.assignment503.repositories;


import com.xsis.assignment503.models.rentalModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.Optional;

public interface rentalRepository extends JpaRepository<rentalModel, Long> {

    @Query(value = "SELECT * FROM rental WHERE reff_code = :code", nativeQuery = true)
    Optional<rentalModel> findByReffCode(String code);

    @Query(value = "SELECT * FROM rental WHERE building_id = :build AND booked_date = :booked",nativeQuery = true)
    Optional<rentalModel> findByDate(Long build, Date booked);

    @Query(value = "SELECT * FROM rental WHERE booked_date = :date AND status = :status",nativeQuery = true)
    Optional<rentalModel> findByDateAndStatus(Date date,String status);
}
