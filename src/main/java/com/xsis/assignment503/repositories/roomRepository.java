package com.xsis.assignment503.repositories;

import com.xsis.assignment503.models.roomModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface roomRepository extends JpaRepository<roomModel, Long> {

    @Query(value = "SELECT * FROM room WHERE name like :name AND building_id = :id", nativeQuery = true)
    List<roomModel> findByName(String name, Long id);

    @Query(value = "SELECT * FROM room WHERE building_id = :id",nativeQuery = true)
    List<roomModel> findByBuildingId(Long id);
}
