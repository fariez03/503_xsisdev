package com.xsis.assignment503.repositories;


import com.xsis.assignment503.models.buildingModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface buildingRepository extends JpaRepository<buildingModel, Long> {

    @Query(value = "SELECT * FROM building WHERE name like :name ",nativeQuery = true)
    List<buildingModel> findByName(String name);

    @Query(value = "SELECT * FROM building WHERE is_active = true" ,nativeQuery = true)
    List<buildingModel> findByActiveStatus();

}
