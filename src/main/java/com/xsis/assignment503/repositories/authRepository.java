package com.xsis.assignment503.repositories;

import com.xsis.assignment503.models.authModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface authRepository extends JpaRepository<authModel,Long> {

    @Query(value = "SELECT * FROM auth WHERE is_active = true", nativeQuery = true)
    List<authModel> getActiveAccount();

    @Query(value = "SELECT * FROM auth WHERE username like :user OR password like :pass ", nativeQuery = true)
    List<authModel> getAccountByUserPass(String user,String pass);

    @Query(value = "SELECT * FROM auth WHERE username like :user AND password like :pass ", nativeQuery = true)
    Optional<authModel> getAccountByOptional(String user, String pass);

    @Query(value = "SELECT * FROM auth WHERE token like :token", nativeQuery = true)
    Optional<authModel> checkToken(String token);
}
