package com.xsis.assignment503.repositories;


import com.xsis.assignment503.models.userDetailModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface userDetailRepository extends JpaRepository<userDetailModel,Long> {
    @Query(value = "SELECT * FROM user_detail WHERE name like :name", nativeQuery = true)
    List<userDetailModel> findByName(String name);

    @Query(value = "SELECT * FROM user_detail WHERE account_id = :id", nativeQuery = true)
    List<userDetailModel> findByAccId(Long id);

}
