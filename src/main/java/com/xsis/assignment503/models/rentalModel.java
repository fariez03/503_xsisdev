package com.xsis.assignment503.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name = "rental")
public class rentalModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String reffCode;

    @OneToOne
    @JoinColumn(name = "building", updatable = false)
    private buildingModel building;

    @Column(nullable = false)
    private Long buildingId;

//    @Column(nullable = false)
//    private Date startDate;
//
//    @Column(nullable = false)
//    private Date endDate;

    @Column(nullable = false)
    private Date bookedDate;

    @Column(nullable = false)
    private Long totalPrice;

    @Column(length = 25, nullable = false)
    private String status;

    @Column(nullable = false)
    private Date createdAt;

    @Column
    private Date modifyAt;

    @OneToOne
    @JoinColumn(name = "customer", updatable = false)
    private authModel customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReffCode() {
        return reffCode;
    }

    public void setReffCode(String reffCode) {
        this.reffCode = reffCode;
    }

    public buildingModel getBuilding() {
        return building;
    }

    public void setBuilding(buildingModel building) {
        this.building = building;
    }

    public Date getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(Date bookedDate) {
        this.bookedDate = bookedDate;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }



    public String getStatus() {
        return status;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public authModel getCustomer() {
        return customer;
    }

    public void setCustomer(authModel customer) {
        this.customer = customer;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifyAt() {
        return modifyAt;
    }

    public void setModifyAt(Date modifyAt) {
        this.modifyAt = modifyAt;
    }
}
