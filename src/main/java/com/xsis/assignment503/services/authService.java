package com.xsis.assignment503.services;

import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.repositories.authRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class authService {
    @Autowired
    private authRepository authRepo;

    public List<authModel> getAllAccount(){
       return authRepo.findAll();
    }

    public List<authModel> getActiveAccount(){
       return authRepo.getActiveAccount();
    }

    public authModel getAccountById(Long id){
        return authRepo.findById(id).get();
    }

    public authModel findAccId(Long id){
        return authRepo.findById(id).orElse(new authModel());
    }

    public List<authModel> findByUserPass(String user, String pass) {
        return authRepo.getAccountByUserPass(user,pass);
    }

    public Optional<authModel> findAcc(String user, String pass){
        return authRepo.getAccountByOptional(user,pass);
    }

    public void addAccount(authModel payload){
        authRepo.save(payload);
    }

    public void updateAccount(authModel payload) {
        authRepo.save(payload);
    }

    public void deleteAccount(authModel payload){
        authRepo.save(payload);
    }

    public Optional<authModel> checkToken(String token){
        return authRepo.checkToken(token);
    }



}
