package com.xsis.assignment503.services;

import com.xsis.assignment503.models.userDetailModel;
import com.xsis.assignment503.repositories.userDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class userDetailService {

    @Autowired
    userDetailRepository userRepo;

    public List<userDetailModel> getAllUser(){
        return userRepo.findAll();
    }

    public userDetailModel getUserById(Long id){
        return userRepo.findById(id).get();
    }

    public List<userDetailModel> findByAccId(Long id){
        return userRepo.findByAccId(id);
    }
    public List<userDetailModel> findByName(String name){
        return userRepo.findByName(name);
    }

    public void addUser(userDetailModel payload) {
        userRepo.save(payload);
    }

    public void updateUser(userDetailModel payload){
        userRepo.save(payload);
    }




}
