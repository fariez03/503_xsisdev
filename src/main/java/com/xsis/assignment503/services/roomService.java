package com.xsis.assignment503.services;

import com.xsis.assignment503.models.buildingModel;
import com.xsis.assignment503.models.roomModel;
import com.xsis.assignment503.repositories.buildingRepository;
import com.xsis.assignment503.repositories.roomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Signature;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class roomService {

    @Autowired
    roomRepository roomRepo;

    @Autowired
    buildingRepository buildRepo;

    public List<roomModel> getAllRoom(){
        return roomRepo.findAll();
    }

    public roomModel getRoomById(Long id){
        return roomRepo.findById(id).orElse(new roomModel());
    }

    public LinkedHashMap<String, Object> getRoomByBuildId(Long id){
        LinkedHashMap<String, Object> response = new LinkedHashMap<>();

        buildingModel data_building = buildRepo.findById(id).orElse(new buildingModel());
        List<roomModel> data_detail = roomRepo.findByBuildingId(id);

            response.put("BuildingName", data_building.getName());
            LinkedHashMap<String, Object> result = new LinkedHashMap<>();
            for(roomModel room : data_detail){
                result.put("id", room.getBuildingId());
                result.put("name", room.getName());
                result.put("address", room.getPrice());
            }
            response.put("dataList", result);

            return response;
    }

    public List<roomModel> getRoomByName(String name,Long id){
        return roomRepo.findByName(name,id);
    }

    public void addRoom(roomModel payload){
        roomRepo.save(payload);
    }

    public void updateRoom(roomModel payload, Long id){
        payload.setId(id);
        roomRepo.save(payload);
    }


}
