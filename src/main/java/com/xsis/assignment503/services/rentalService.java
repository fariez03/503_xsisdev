package com.xsis.assignment503.services;

import com.xsis.assignment503.models.rentalModel;
import com.xsis.assignment503.repositories.rentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class rentalService {

    @Autowired
    rentalRepository rentRepo;

    public List<rentalModel> getAllrental(){
        return rentRepo.findAll();
    }

    public Optional<rentalModel> getDataByReferralCode(String id){
        return rentRepo.findByReffCode(id);
    }

    public Optional<rentalModel> getByBuildAndDate(Long id, Date date){
        return rentRepo.findByDate(id,date);
    }

    public Optional<rentalModel> getByDateAndStatus(Date date, String status){
        return rentRepo.findByDateAndStatus(date,status);
    }

    public void addRental(rentalModel payload){
        rentRepo.save(payload);
    }

    public void updateRental(rentalModel payload){
//        payload.setId();
        rentRepo.save(payload);
    }




}
