package com.xsis.assignment503.services;


import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.models.buildingModel;
import com.xsis.assignment503.repositories.buildingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class buildingService {

    @Autowired
    buildingRepository buildRepo;


    public List<buildingModel> getAll(){
        return buildRepo.findAll();
    }

    public List<buildingModel> getBuildingActive(){
        return buildRepo.findByActiveStatus();
    }

    public buildingModel getBuildingById(Long id){
        return buildRepo.findById(id).orElse(new buildingModel());
    }

    public List<buildingModel> getBuildByName(String name){
        return buildRepo.findByName(name);
    }

    public void addBuilding(buildingModel payload){
        buildRepo.save(payload);
    }

    public void updateBuilding(buildingModel payload){
        buildRepo.save(payload);
    }

}
