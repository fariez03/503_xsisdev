package com.xsis.assignment503;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment503Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment503Application.class, args);
	}

}
