package com.xsis.assignment503.controllers;

import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.models.userDetailModel;
import com.xsis.assignment503.services.authService;
import com.xsis.assignment503.services.userDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class userDetailRestController {

    @Autowired
    private userDetailService userService;
    @Autowired
    private authService authService;

    @GetMapping("user/all")
    public List<userDetailModel> getAll(@RequestHeader String token){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }
        return userService.getAllUser();
    }

    @GetMapping("user/{id}")
    public userDetailModel getById(@RequestHeader String token, @PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }
        return userService.getUserById(id);
    }

    @PostMapping("user/add")
    public String addUser(@RequestHeader String token, @RequestBody userDetailModel payload){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        authModel checkAccount = authService.findAccId(payload.getAccountId());
        if(checkAccount.getId() == null){
            return "Account Tidak Ditemukan";
        }else{

            List<userDetailModel> checkAccId = userService.findByAccId(payload.getAccountId());
            if (checkAccId.size() > 0) {
                return "Id account Telah Digunakan";
            }

            List<userDetailModel> checkName = userService.findByName(payload.getName());
            if (checkName.size() > 0) {
                return "Nama Telah Digunakan";
            }

            List<userDetailModel> checkEmail = userService.findByName(payload.getEmail());
            if (checkEmail.size() > 0) {
                return "Email Telah Digunakan";
            }

            payload.setAccount(checkAccount);
            payload.setAccountId(payload.getAccountId());
            payload.setName(payload.getName());
            payload.setEmail(payload.getEmail());
            payload.setPhone(payload.getPhone().toString());
            payload.setAddress(payload.getAddress());
            payload.setCreatedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

            userService.addUser(payload);
            return "Data Pengguna telah disimpan";
        }


    }

    @PutMapping("user/update/{id}")
    public String updateUser(@RequestHeader String token,@RequestBody userDetailModel payload, @PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        List<userDetailModel> checkAccId = userService.findByAccId(payload.getAccountId());
        if (checkAccId.size() > 0) {
            return "Id account Telah Digunakan";
        }

        List<userDetailModel> checkName = userService.findByName(payload.getName());
        if (checkName.size() > 0) {
            return "Nama Telah Digunakan";
        }

        List<userDetailModel> checkEmail = userService.findByName(payload.getEmail());
        if (checkEmail.size() > 0) {
            return "Email Telah Digunakan";
        }

        userDetailModel data_detail = userService.getUserById(id);
        authModel accountData = authService.getAccountById(data_detail.getAccountId());

        payload.setId(id);
        payload.setAccount(accountData);
        payload.setAccountId(data_detail.getAccountId());
        payload.setName(payload.getName());
        payload.setEmail(payload.getEmail());
        payload.setPhone(payload.getPhone().toString());
        payload.setAddress(payload.getAddress());
        payload.setCreatedAt(data_detail.getCreatedAt());
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        userService.updateUser(payload);
        return "Data User Telah Diubah";

    }


}
