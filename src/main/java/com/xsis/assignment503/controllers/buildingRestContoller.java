package com.xsis.assignment503.controllers;


import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.models.buildingModel;
import com.xsis.assignment503.services.authService;
import com.xsis.assignment503.services.buildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class buildingRestContoller {

    @Autowired
    private buildingService buildService;

    @Autowired
    private authService authService;

    @GetMapping("building/all")
    public List<buildingModel> getAll(@RequestHeader String token){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }
        return buildService.getBuildingActive();
    }

    @GetMapping("building/{id}")
    public buildingModel getById(@RequestHeader String token,@PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }
        return buildService.getBuildingById(id);
    }

    @PostMapping("building/add")
    public String addBuilding (@RequestHeader String token,@RequestBody buildingModel payload){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        List<buildingModel> nameCheck = buildService.getBuildByName(payload.getName());
        if(nameCheck.size() > 0){
            return "Nama Telah Digunakan";
        }

        payload.setName(payload.getName());
        payload.setAddress(payload.getAddress());
        payload.setOwner(payload.getOwner());
        payload.setPhone(payload.getPhone().toString());
        payload.setActive(true);
        payload.setCreatedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        buildService.addBuilding(payload);
        return "Data Gedung Telah Disimpan";
    }

    @PutMapping("building/update/{id}")
    public String updateBuilding (@RequestHeader String token,@RequestBody buildingModel payload, @PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }


        List<buildingModel> nameCheck = buildService.getBuildByName(payload.getName());
        if(nameCheck.size() > 0){
            return "Nama Telah Digunakan";
        }

        buildingModel data_detail = buildService.getBuildingById(id);

        payload.setId(id);
        payload.setName(payload.getName() == null ? data_detail.getName() : payload.getName());
        payload.setAddress(payload.getAddress() == null ? data_detail.getAddress() : payload.getAddress());
        payload.setOwner(payload.getOwner() == null ? data_detail.getOwner() : payload.getOwner());
        payload.setPhone(payload.getPhone() == null ? data_detail.getPhone() : payload.getPhone());
        payload.setActive(data_detail.getActive());
        payload.setCreatedAt(data_detail.getCreatedAt());
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        buildService.updateBuilding(payload);
        return "Data Gedung Telah Diubah";

    }

    @PutMapping("building/delete/{id}")
    public String deleteBuilding (@RequestHeader String token,@PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        buildingModel payload = buildService.getBuildingById(id);

        payload.setId(id);
        payload.setActive(false);
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        buildService.updateBuilding(payload);
        return "Data Gedung Telah Dinonaktifkan";
    }



}
