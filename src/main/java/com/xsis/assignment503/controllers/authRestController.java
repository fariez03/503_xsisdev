package com.xsis.assignment503.controllers;

import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.services.authService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api")
public class authRestController {

    @Autowired
    private authService authService;

    @GetMapping("account")
    public List<authModel> getActive(){
        return authService.getActiveAccount();
    }

    @GetMapping("account/all")
    public List<authModel> getAll(){
        return authService.getAllAccount();
    }

    @GetMapping("account/{id}")
    public authModel getById(@PathVariable Long id){
        return authService.getAccountById(id);
    }

    @PostMapping("account/add")
    public String addAcc(@RequestBody authModel payload){
        List<authModel> data = authService.findByUserPass(payload.getUsername(),payload.getPassword());
        if (data.size()>0) {
            return "Username/Password Telah Dipakai!";
        }

        payload.setActive(true);
        payload.setCreatedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        authService.addAccount(payload);
        return "Akun telah ditambahkan";
    }

    @PutMapping("account/update/{id}")
    public String updateAcc(@RequestBody authModel payload,@PathVariable Long id){
        authModel data_detail = authService.getAccountById(id);

        List<authModel> getUsername = authService.findByUserPass(payload.getUsername(),payload.getPassword());
        if (getUsername.size() > 0 && !payload.getUsername().equals(data_detail.getUsername()) && !payload.getPassword().equals(data_detail.getPassword())) {
            return "Username / Password Telah Digunakan";
        }

        payload.setId(id);
        payload.setUsername(payload.getUsername());
        payload.setPassword(payload.getPassword());
        payload.setActive(data_detail.getActive());
        payload.setRole(data_detail.getRole());
        payload.setCreatedAt(data_detail.getCreatedAt());
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        authService.updateAccount(payload);
        return "Akun telah diubah";
    }

    @PutMapping("account/delete/{id}")
    public String deleteAcc(@PathVariable Long id){
        authModel payload = authService.getAccountById(id);

        payload.setId(id);
        payload.setActive(false);

        authService.updateAccount(payload);
        return "Akun telah dihapus";
    }

    @PostMapping("login")
    public LinkedHashMap<String,Object> login(@RequestBody authModel payload){

        Optional<authModel> checkAcc = authService.findAcc(payload.getUsername(),payload.getPassword());
        LinkedHashMap<String, Object> response = new LinkedHashMap<>();

        if (checkAcc.isPresent()) {

            String token = "token-"+UUID.randomUUID().toString();

            authModel data = checkAcc.get();
            data.setToken(token);
            data.setLastLogin(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

            authService.addAccount(data);

            response.put("message","Anda Berhasil Login");
            response.put("token", token);


        }else {
            response.put("message","Username & password anda tidak cocok");
        }
        return response;
    }


}
