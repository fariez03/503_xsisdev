package com.xsis.assignment503.controllers;

import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.models.buildingModel;
import com.xsis.assignment503.models.rentalModel;
import com.xsis.assignment503.services.authService;
import com.xsis.assignment503.services.buildingService;
import com.xsis.assignment503.services.rentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api")
public class rentalRestController {

    @Autowired
    private authService authService;

    @Autowired
    private rentalService rentService;

    @Autowired
    private buildingService buildService;

    @GetMapping("rental/all")
    public List<rentalModel> getAllRental(@RequestHeader String token){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        return rentService.getAllrental();
    }

    @GetMapping("rental/{date}/{status}")
    public Optional<rentalModel> getRentalByDateStatus(@PathVariable Date date,@PathVariable String status){
        return rentService.getByDateAndStatus(date,status);
    }

    @PostMapping("rental/custom")
    public Optional<rentalModel> getCustomRental(@RequestBody rentalModel payload){

        return rentService.getByDateAndStatus(payload.getBookedDate(),payload.getStatus());
    }

    @PostMapping("rental")
    public String rental(@RequestHeader String token,@RequestBody rentalModel payload){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        if (payload.getBookedDate().before(new java.sql.Date(Calendar.getInstance().getTimeInMillis()))){
            return "Tanggal harus lebih dari tanggal hari ini";
        }

        Optional<rentalModel> checkDate = rentService.getByBuildAndDate(payload.getBuildingId(), payload.getBookedDate());
        if (checkDate.isPresent()){
            return "Mohon maaf pada tanggal tersebut gedung sedang dipesan";
        }

        buildingModel data_building = buildService.getBuildingById(payload.getBuildingId());
        authModel data_customer = checkSession.get();

        payload.setBuilding(data_building);
        payload.setBuildingId(payload.getBuildingId());
        payload.setReffCode(UUID.randomUUID().toString());
        payload.setBookedDate(payload.getBookedDate());
        payload.setTotalPrice(payload.getTotalPrice());
        payload.setCustomer(data_customer);
        payload.setStatus("Belum Bayar");
        payload.setCreatedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        rentService.addRental(payload);
        return "Pesanan telah dibuat";
    }

    @PostMapping("payment")
    public String payment(@RequestHeader String token,@RequestBody rentalModel payload){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        Optional<rentalModel> reffCodeCheck = rentService.getDataByReferralCode(payload.getReffCode());
        if (!reffCodeCheck.isPresent()){
            return "Kode Refferal yang anda masukkan tidak ditemukan";
        }else {
            rentalModel data = reffCodeCheck.get();
            if(data.getTotalPrice() > payload.getTotalPrice()) {
                return "Mohon maaf uang anda kurang";
            }else {
                data.setId(data.getId());
                data.setStatus("Telah Dibayar");
                data.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
                rentService.updateRental(data);
//                Long kembalian = payload.getTotalPrice() - data.getTotalPrice();
                return "pembayaran anda telah selesai";

            }
        }
    }



}
