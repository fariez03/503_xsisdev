package com.xsis.assignment503.controllers;

import com.xsis.assignment503.models.authModel;
import com.xsis.assignment503.models.buildingModel;
import com.xsis.assignment503.models.roomModel;
import com.xsis.assignment503.services.authService;
import com.xsis.assignment503.services.buildingService;
import com.xsis.assignment503.services.roomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class roomRestController {

    @Autowired
    private authService authService;

    @Autowired
    private roomService roomService;

    @Autowired
    private buildingService buildService;


    @GetMapping("room/all")
    public List<roomModel> getAll(@RequestHeader String token){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        return roomService.getAllRoom();
    }

    @GetMapping("room/{id}")
    public roomModel getById(@RequestHeader String token,@PathVariable Long id){

        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        return roomService.getRoomById(id);
    }

    @GetMapping("room/building/{id}")
    public LinkedHashMap<String, Object> getAll(@RequestHeader String token,@PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        return roomService.getRoomByBuildId(id);
    }

    @PostMapping("room/add")
    public String addRoom (@RequestHeader String token,@RequestBody roomModel payload){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        List<roomModel> nameCheck = roomService.getRoomByName(payload.getName(),payload.getBuildingId());
        if (nameCheck.size() > 0) {
            return "Nama ruang telah digunakan pada gedung yang sama";
        }

        buildingModel data_building = buildService.getBuildingById(payload.getBuildingId());

        payload.setBuilding(data_building);
        payload.setBuildingId(payload.getBuildingId());
        payload.setName(payload.getName());
        payload.setPrice(new BigDecimal(payload.getPrice().toString()));
        payload.setInformation(payload.getInformation());
        payload.setActive(true);
        payload.setCreatedAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        roomService.addRoom(payload);
        return "Data ruangan telah disimpan";
    }

    @PutMapping("room/update/{id}")
    public String updateRoom (@RequestHeader String token,@RequestBody roomModel payload,@PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        List<roomModel> nameCheck = roomService.getRoomByName(payload.getName(),payload.getBuildingId());
        if (nameCheck.size() > 0) {
            return "Nama ruang telah digunakan pada gedung yang sama";
        }

        roomModel data_detail = roomService.getRoomById(id);
        buildingModel data_building = buildService.getBuildingById(data_detail.getBuildingId());

        payload.setBuilding(data_building);
        payload.setBuildingId(data_detail.getBuildingId());
        payload.setName(payload.getName() == null ? data_detail.getName() : payload.getName());
        payload.setPrice(new BigDecimal(payload.getPrice().toString() == null ? data_detail.getPrice().toString() : payload.getPrice().toString()));
        payload.setInformation(payload.getInformation() == null ? data_detail.getInformation() : payload.getInformation());
        payload.setActive(payload.getActive() == null ? data_detail.getActive() : payload.getActive());
        payload.setCreatedAt(data_detail.getCreatedAt());
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        roomService.updateRoom(payload,id);
        return "Data ruangan telah diubah";
    }

    @PutMapping("room/delete/{id}")
    public String deleteRoom(@RequestHeader String token,@PathVariable Long id){
        Optional<authModel> checkSession = authService.checkToken(token);
        if (!checkSession.isPresent()){
            throw new RuntimeException("Invalid Token");
        }

        roomModel payload = roomService.getRoomById(id);

        payload.setActive(false);
        payload.setModifyAt(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        roomService.updateRoom(payload, id);
        return "Data ruangan telah dinonaktifkan";
    }

}
